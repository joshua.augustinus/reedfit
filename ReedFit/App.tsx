import React from 'react';
import {SignUp} from '@components/SignUp';
import {Home} from '@components/Home';
import {Login} from '@components/Login';
import AppHeader from '@components/AppHeader';
import {LoggedInHome} from '@components/LoggedInHome';
import {MobilePhone} from '@components/MobilePhone';
import {createStackNavigator} from '@react-navigation/stack';

import {SafeAreaView, LogBox} from 'react-native';

const Stack = createStackNavigator();

// ignore specific yellowbox warnings
LogBox.ignoreLogs(['Require cycle:', 'Remote debugger']);

const App = (props: any) => {
  return (
    <SafeAreaView style={{flex: 1}}>
      <AppHeader />

      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="SignUp" component={SignUp} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="LoggedInHome" component={LoggedInHome} />
        <Stack.Screen name="MobilePhone" component={MobilePhone} />
      </Stack.Navigator>
    </SafeAreaView>
  );
};

export default App;
