/**
 * @format
 */
import 'react-native-gesture-handler'; //required by react-navigation
import React from 'react';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import {NavigationContainer} from '@react-navigation/native';
import {Colors} from '@src/colors';

const theme = {
  ...DefaultTheme,
  // Specify custom property

  // Specify custom property in nested object
  colors: {
    ...DefaultTheme.colors,
    background: Colors.background,
    primary: Colors.primary,
  },
};

const Root = (props) => (
  <PaperProvider theme={theme}>
    <NavigationContainer theme={theme}>
      <App {...props} />
    </NavigationContainer>
  </PaperProvider>
);

AppRegistry.registerComponent(appName, () => Root);
