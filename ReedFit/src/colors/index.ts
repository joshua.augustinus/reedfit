const Colors = {
  background: '#f5f8fb',
  primary: '#5b6ff3',
  button: {
    primary: '#fcde00',
    light: 'white',
    secondary: '#5b6ff3',
  },
};

export {Colors};
