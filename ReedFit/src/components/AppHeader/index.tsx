import React from 'react';
import {Text, Title, useTheme} from 'react-native-paper';

import styles from './styles';

import {View, TouchableOpacity} from 'react-native';
import {Logo} from '../Logo';

const AppHeader = () => {
  const {colors} = useTheme();

  return (
    <View style={{...styles.container, backgroundColor: colors.primary}}>
      <View style={styles.svgWrapper}>
        <Logo />
      </View>
    </View>
  );
};

export default AppHeader;
