import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: 60,
    paddingHorizontal: 20,
    alignItems: 'flex-start',
  },
  headerText: {
    color: 'white',
  },
  subtitleText: {
    opacity: 0.7,
    color: 'white',
    marginBottom: 5,
    flex: 1,
  },
  subtitleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flex: 1,

    width: '100%',
  },
  svgWrapper: {
    aspectRatio: 1,
    flex: 1,
  },
});

export default styles;
