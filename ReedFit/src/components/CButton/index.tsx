import {Button} from 'react-native-elements';
import React from 'react';
import {Colors} from '@src/colors';
import {ButtonType} from '@src/types';

interface Props {
  title: string;
  style?: any;
  type?: ButtonType;
  onPress: () => void;
  small?: boolean;
}

const CButton = (props: Props) => {
  let backgroundColor = Colors.button.primary;
  if (props.type) {
    backgroundColor = Colors.button[props.type];
  }

  const titleColor = props.type === 'secondary' ? 'white' : 'black';
  const fontSize = props.small ? 15 : 25;
  return (
    <Button
      raised
      title={props.title}
      buttonStyle={{
        borderRadius: 5,
        backgroundColor: backgroundColor,
        paddingVertical: 15,
      }}
      titleStyle={{
        color: titleColor,
        fontSize: fontSize,
        fontWeight: 'normal',
      }}
      containerStyle={{
        ...props.style,
      }}
      onPress={props.onPress}
    />
  );
};

export {CButton};
