import React, {useState, ReactNode} from 'react';
import {Card} from 'react-native-paper';
import styles from './styles';
import {View} from 'react-native';

interface Props {
  children: ReactNode;
  style?: any;
  fill?: boolean;
}

const CCard = (props: Props) => {
  if (props.fill) {
    return (
      <Card
        style={{
          ...styles.card,
          ...props.style,
          flex: 1,
          marginBottom: styles.card.marginTop,
        }}>
        {props.children}
      </Card>
    );
  } else {
    return (
      <Card style={{...styles.card, ...props.style}}>
        <Card.Content>{props.children}</Card.Content>
      </Card>
    );
  }
};

export {CCard};
