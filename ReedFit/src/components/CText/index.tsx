import React, {ReactNode} from 'react';
import {Text} from 'react-native';
import {useWindowDimensions} from 'react-native';

import {Size} from '@src/sizes';

interface Props {
  style: any;
  children: ReactNode;
}

const CText = (props: Props) => {
  const windowWidth = useWindowDimensions().width;
  let fontSize = props.style.fontSize;
  if (windowWidth <= Size.SMALL_WIDTH && fontSize)
    fontSize = Math.round(props.style.fontSize * 0.7);

  return (
    <Text style={{...props.style, fontSize: fontSize}}>{props.children}</Text>
  );
};

export {CText};
