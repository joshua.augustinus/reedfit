import React from 'react';

import styles from './styles';

import LinearGradient from 'react-native-linear-gradient';

const GradientBackground = (props: any) => {
  return (
    <LinearGradient
      colors={['#5b6ff3', '#4821c2']}
      style={styles.linearGradient}>
      {props.children}
    </LinearGradient>
  );
};

export {GradientBackground};
