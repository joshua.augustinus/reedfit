import React from 'react';
import {View, Text, Image, ImageSourcePropType} from 'react-native';

import styles from './styles';

import {GradientBackground} from './GradientBackground';
import {HomeScreenData} from '@src/types';
import {CText} from '@components/CText';

interface Props {
  data: HomeScreenData;
}

const SwipableScreen = (props: Props) => {
  return (
    <GradientBackground>
      <View style={{flex: 2}}>
        <Image
          style={{flex: 1}}
          resizeMode="contain"
          source={props.data.imageSource}
        />
      </View>
      <View style={{flex: 1, justifyContent: 'center'}}>
        <CText style={styles.textHeading}>{props.data.heading}</CText>
      </View>
      <View style={{flex: 2}}>
        {props.data.content.map((value, index) => {
          return (
            <CText key={index} style={styles.text}>
              {value}
            </CText>
          );
        })}
      </View>
      <View style={styles.bottomView} />
    </GradientBackground>
  );
};

export {SwipableScreen};
