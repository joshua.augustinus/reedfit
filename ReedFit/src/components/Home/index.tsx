import React from 'react';
import {View, Text, ColorValue} from 'react-native';
import Swiper from 'react-native-swiper';
import styles from './styles';
import {CButton} from '../CButton';
import {GradientBackground} from './GradientBackground';
import * as Animatable from 'react-native-animatable';
import {SwipableScreen} from './SwipableScreen';
import {useNavigation} from '@react-navigation/native';
import {Logo} from '../Logo';
import {Colors} from '@src/colors';
import {CText} from '@components/CText';

const dot = (color: ColorValue) => {
  return (
    <View
      style={{
        backgroundColor: color,
        width: 8,
        height: 8,
        borderRadius: 4,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3,
      }}
    />
  );
};

const Home = () => {
  const navigation = useNavigation();
  const screen2 = {
    content: ['Upload a bill.', 'We pay it.', 'Pay it back in 4 installments.'],
    heading: 'How it works',
    imageSource: require('../../images/billies_paid.png'),
  };

  const screen3 = {
    heading: 'Bills made easy',
    content: [
      'Split any bill in 4 instalments.',
      'Free up cashflow.',
      'Stay in control.',
    ],
    imageSource: require('../../images/my-bills.png'),
  };

  const screen4 = {
    content: [
      '$5.99 for up to $2,000 balance.',
      'No interest.',
      'No late fees. Ever.',
    ],
    heading: 'Simple',
    imageSource: require('../../images/billie_jumping_upload.png'),
  };

  const signupHandler = () => {
    navigation.navigate('SignUp');
  };

  const loginHandler = () => {
    navigation.navigate('Login');
  };

  return (
    <View style={{flex: 1}}>
      <Swiper
        dot={dot(Colors.background)}
        activeDot={dot(Colors.button.primary)}
        autoplayTimeout={4}
        style={styles.wrapper}
        showsButtons={false}
        loop={false}
        autoplay={true}>
        <GradientBackground>
          <View style={styles.svgWrapper}>
            <Animatable.View animation="bounceInLeft" delay={800}>
              <Logo />
            </Animatable.View>
          </View>
          <View style={{flex: 2}}>
            <CText style={styles.text}>
              We pay your bills. Interest free. No hidden or late fees. Ever.
            </CText>
          </View>
          <View style={styles.bottomView} />
        </GradientBackground>
        <SwipableScreen data={screen2} />
        <SwipableScreen data={screen3} />
        <SwipableScreen data={screen4} />
      </Swiper>
      <View style={styles.bottomFloat}>
        <CButton
          title="Sign Up"
          style={styles.button}
          onPress={signupHandler}
        />
        <CButton
          title="Login"
          style={styles.button}
          type="light"
          onPress={loginHandler}
        />
      </View>
    </View>
  );
};

export {Home};
