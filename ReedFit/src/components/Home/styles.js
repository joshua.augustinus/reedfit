import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  wrapper: {},
  linearGradient: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 30,
  },
  svgWrapper: {
    flex: 2,
    aspectRatio: 1,
  },
  text: {
    color: '#fff',
    fontSize: 25,
    textAlign: 'center',
  },
  textHeading: {
    color: '#fff',
    fontSize: 40,
    textAlign: 'center',
    marginBottom: 10,
    marginTop: 10,
  },
  bottomFloat: {
    left: 0,
    right: 0,
    bottom: 0,
    position: 'absolute',
    marginBottom: 50,
    display: 'flex',
    alignContent: 'center',
    alignItems: 'center',
  },
  button: {
    marginTop: 15,
    width: '80%',
  },
  bottomView: {
    height: 140,
    backgroundColor: 'red',
  },
});

export default styles;
