import React, {useState} from 'react';
import * as Animatable from 'react-native-animatable';
import {CButton} from '@src/components/CButton';

interface Props {
  style: any;
  title: string;
  onPress: () => void;
  index: number;
}

const AccountButton = (props: Props) => {
  const delay = props.index % 3 === 0 ? 500 : 800;

  const animation = props.index % 2 === 0 ? 'bounceInLeft' : 'bounceInRight';

  return (
    <Animatable.View delay={delay} animation={animation} style={{flex: 1}}>
      <CButton
        small
        type="secondary"
        style={props.style}
        title={props.title}
        onPress={props.onPress}
      />
    </Animatable.View>
  );
};

export {AccountButton};
