import React, {useState} from 'react';
import {CCard} from '@src/components/CCard';

import {View, Text, ScrollView} from 'react-native';

import {ListItem} from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Title} from 'react-native-paper';
import {useNavigation} from '@react-navigation/native';
import {AccountButton} from './AccountButton';
import {CButton} from '@src/components/CButton';

const Account = () => {
  const navigation = useNavigation();

  const buttonStyle = {
    flex: 1,
    marginBottom: 5,
  };

  //icons:
  //https://oblador.github.io/react-native-vector-icons/
  const list = [
    {
      title: 'Cards',
      icon: 'credit-card',
    },
    {
      title: 'Settings',
      icon: 'cog',
    },
    {
      title: 'Terms',
      icon: 'file-document-edit-outline',
    },
    {
      title: 'Privacy Policy',
      icon: 'file-document-outline',
    },
  ];

  const logoutHandler = () => {
    navigation.reset({
      index: 0,
      routes: [{name: 'Home'}],
    });
  };

  return (
    <ScrollView>
      <CCard>
        <Title>Joshua Augustinus</Title>
        <Text>joshua.augustinus@gmail.com</Text>
        <Text>0423298848</Text>
        <Text>Balance Available: $0</Text>
      </CCard>
      <CCard>
        <CButton
          small
          onPress={() => {}}
          title="Have a referral code?"
          type="light"
        />
        <View style={{flexDirection: 'row', flexWrap: 'wrap', marginTop: 10}}>
          <AccountButton
            index={0}
            style={buttonStyle}
            title="How To"
            onPress={() => {}}
          />
          <View style={{width: 5}} />
          <AccountButton
            index={1}
            style={buttonStyle}
            title="Help Center"
            onPress={() => {}}
          />
        </View>
        <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
          <AccountButton
            index={2}
            style={buttonStyle}
            title="Get In Touch"
            onPress={() => {}}
          />
          <View style={{width: 5}} />
          <AccountButton
            index={3}
            style={buttonStyle}
            title="Logout"
            onPress={logoutHandler}
          />
        </View>
        <View style={{marginTop: 10}}>
          {list.map((item, i) => (
            <ListItem key={i} bottomDivider onPress={() => {}}>
              <Icon name={item.icon} size={20} style={{opacity: 0.5}} />
              <ListItem.Content>
                <ListItem.Title style={{paddingVertical: 5}}>
                  {item.title}
                </ListItem.Title>
              </ListItem.Content>
              <ListItem.Chevron />
            </ListItem>
          ))}
        </View>
      </CCard>
      <View style={{height: 20}}></View>
    </ScrollView>
  );
};

export {Account};
