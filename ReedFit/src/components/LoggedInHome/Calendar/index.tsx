import React, {useState} from 'react';
import {CCard} from '@src/components/CCard';

import {View, Text} from 'react-native';
import styles from '../styles';

const Calendar = () => {
  return (
    <CCard fill>
      <View style={{justifyContent: 'center', flex: 1, padding: 30}}>
        <Text style={styles.title}>
          You have no upcoming payments in the next 30 days
        </Text>
      </View>
    </CCard>
  );
};

export {Calendar};
