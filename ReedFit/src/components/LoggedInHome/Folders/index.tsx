import React, {useState} from 'react';
import {CCard} from '@src/components/CCard';

import styles from '../styles';
import {Text, View} from 'react-native';

const Folders = () => {
  return (
    <CCard fill>
      <View style={styles.wrapper}>
        <Text style={styles.title}>
          You don't have any bills in this folder
        </Text>
      </View>
    </CCard>
  );
};

export {Folders};
