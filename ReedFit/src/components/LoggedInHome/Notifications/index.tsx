import React, {useState} from 'react';
import {CCard} from '@src/components/CCard';
import styles from '../styles';
import {Text, View} from 'react-native';
const Notifications = () => {
  return (
    <CCard fill>
      <View style={styles.wrapper}>
        <Text style={styles.title}>You have no notifications</Text>
      </View>
    </CCard>
  );
};

export {Notifications};
