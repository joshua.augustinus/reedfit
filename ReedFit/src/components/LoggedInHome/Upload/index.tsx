import React, {useState} from 'react';
import {CCard} from '@src/components/CCard';
import styles from '../styles';
import {Text, View, Image} from 'react-native';
import * as Animatable from 'react-native-animatable';

const Upload = () => {
  return (
    <CCard fill>
      <View style={styles.wrapper}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Animatable.Image
            animation="bounceInRight"
            delay={600}
            resizeMode="contain"
            source={require('../../../images/plane.png')}
          />
        </View>
        <View style={{flex: 1}}>
          <Text style={styles.title}>
            Upload a file or take a photo of your bill
          </Text>
        </View>
      </View>
    </CCard>
  );
};

export {Upload};
