import React from 'react';
import {Folders} from './Folders';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Calendar} from './Calendar';
import {Upload} from './Upload';
import {Notifications} from './Notifications';
import {Account} from './Account';

const Bottom = createMaterialBottomTabNavigator();

const LoggedInHome = (props: any) => {
  return (
    <Bottom.Navigator initialRouteName="Upload" backBehavior="none">
      <Bottom.Screen
        name="Folders"
        component={Folders}
        options={{
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="folder" color={color} size={26} />
          ),
        }}
      />
      <Bottom.Screen
        name="Calendar"
        component={Calendar}
        options={{
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="calendar" color={color} size={26} />
          ),
        }}
      />
      <Bottom.Screen
        name="Upload"
        component={Upload}
        options={{
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons
              name="cloud-upload"
              color={color}
              size={26}
            />
          ),
        }}
      />
      <Bottom.Screen
        name="Notifications"
        component={Notifications}
        options={{
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="bell" color={color} size={26} />
          ),
        }}
      />
      <Bottom.Screen
        name="Account"
        component={Account}
        options={{
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="account" color={color} size={26} />
          ),
        }}
      />
    </Bottom.Navigator>
  );
};

export {LoggedInHome};
