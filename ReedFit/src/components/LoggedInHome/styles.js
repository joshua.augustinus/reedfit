import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  card: {
    flex: 1,
    justifyContent: 'center',
  },
  wrapper: {justifyContent: 'center', flex: 1, margin: 5},
  title: {alignSelf: 'center', fontSize: 18, textAlign: 'center'},
});

export default styles;
