import React, {useState} from 'react';
import {Text} from 'react-native';

import {CButton} from '@components/CButton';
import {TextInput, Card, Title} from 'react-native-paper';
import styles from './styles';

import {CPassword} from '../CPassword';
import {CCard} from '../CCard';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';

const Login = () => {
  const navigation = useNavigation();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const loginHandler = () => {
    navigation.reset({
      index: 0,
      routes: [{name: 'LoggedInHome'}],
    });
  };

  return (
    <CCard>
      <Title style={styles.title}>Sign In</Title>
      <TextInput
        style={styles.input}
        label="Email Address"
        value={email}
        onChangeText={(text) => setEmail(text)}
      />
      <CPassword
        style={styles.input}
        label="Password"
        value={password}
        onChangeText={(text) => setPassword(text)}
      />

      <CButton title="Login" onPress={loginHandler} style={{margin: 20}} />
      <TouchableOpacity>
        <Text style={styles.forgotPassword}>Forgot password?</Text>
      </TouchableOpacity>
    </CCard>
  );
};

export {Login};
