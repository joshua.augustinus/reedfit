import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  input: {
    marginBottom: 10,
  },
  title: {
    alignSelf: 'center',
    marginBottom: 10,
  },
  forgotPassword: {
    marginTop: 5,
    alignSelf: 'flex-end',
    textDecorationLine: 'underline',
  },
});

export default styles;
