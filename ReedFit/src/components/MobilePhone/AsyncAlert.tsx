import {Alert} from 'react-native';

const AsyncAlert = (title: string, msg: string) =>
  new Promise((resolve) => {
    Alert.alert(
      title,
      msg,
      [
        {
          text: 'ok',
          onPress: () => {
            resolve('YES');
          },
        },
      ],
      {cancelable: false},
    );
  });

export {AsyncAlert};
