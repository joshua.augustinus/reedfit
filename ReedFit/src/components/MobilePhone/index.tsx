import React, {useState, useRef, useEffect} from 'react';

import {View, StyleSheet, Text, Alert} from 'react-native';
import {TextInput} from 'react-native-paper';
import CountryPicker, {
  Country,
  CountryCode,
} from 'react-native-country-picker-modal';
import {CCard} from '../CCard';
import {CButton} from '../CButton';
import styles from './styles';
import {useNavigation} from '@react-navigation/native';
import {AsyncAlert} from './AsyncAlert';
import {getCountryCallingCodeAsync} from 'react-native-country-picker-modal/lib/CountryService';
import * as RNLocalize from 'react-native-localize';

const getCountryCode = () => {
  const locales = RNLocalize.getLocales();
  console.log(locales[0]);
  const result = locales[0].countryCode as CountryCode;
  return result;
};

const MobilePhone = () => {
  const navigation = useNavigation();
  const [phone, setPhone] = useState('');
  const [callingCode, setCallingCode] = useState('');
  const [cca2, setCca2] = useState<CountryCode>(getCountryCode());

  const selectCountry = (country: Country) => {
    setCca2(country.cca2);
    setCallingCode('+' + country.callingCode.toString());
    console.log(country);
  };

  const onVerify = async () => {
    const message =
      'SMS feature not yet implemented. Redirecting to Login page.';
    await AsyncAlert(undefined, message);
    navigation.navigate('Login');
  };

  useEffect(() => {
    getCountryCallingCodeAsync(cca2).then((callingCode) => {
      setCallingCode('+' + callingCode);
    });
  }, []);

  return (
    <CCard>
      <Text>
        Please enter your mobile phone number below to verify with an SMS code.
      </Text>
      <View style={styles.mobileRow}>
        <CountryPicker
          containerButtonStyle={{marginBottom: 10}}
          onSelect={(value) => selectCountry(value)}
          translation="common"
          countryCode={cca2}
        />
        <Text style={{width: 40, marginBottom: 15}}>{callingCode}</Text>
        <TextInput
          keyboardType="phone-pad"
          style={{flex: 1}}
          textContentType="telephoneNumber"
          label="Mobile Phone Number"
          value={phone}
          onChangeText={(text) => setPhone(text)}
        />
      </View>
      <CButton title="Verify" onPress={onVerify} />
    </CCard>
  );
};

export {MobilePhone};
