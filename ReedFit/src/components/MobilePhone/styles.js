import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  mobileRow: {
    flexDirection: 'row',

    alignItems: 'flex-end',
    padding: 10,
    height: 100,
    marginBottom: 20,
  },
});

export default styles;
