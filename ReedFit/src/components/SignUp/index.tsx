import React, {useState} from 'react';
import {Text} from 'react-native';

import {CButton} from '@components/CButton';
import {TextInput, Card} from 'react-native-paper';
import styles from './styles';
import {CheckBox} from 'react-native-elements';
import {CPassword} from '../CPassword';
import {useNavigation} from '@react-navigation/native';

const SignUp = () => {
  const navigation = useNavigation();
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [accept, setAccept] = useState(false);

  const signUpHandler = () => {
    navigation.navigate('MobilePhone');
  };

  return (
    <Card style={styles.card}>
      <Card.Content>
        <TextInput
          style={styles.input}
          label="First Name"
          value={firstName}
          onChangeText={(text) => setFirstName(text)}
        />
        <TextInput
          style={styles.input}
          label="Last Name"
          value={lastName}
          onChangeText={(text) => setLastName(text)}
        />
        <TextInput
          style={styles.input}
          label="Email Address"
          value={email}
          onChangeText={(text) => setEmail(text)}
        />
        <CPassword
          style={styles.input}
          label="Password"
          value={password}
          onChangeText={(text) => setPassword(text)}
        />
        <CheckBox
          containerStyle={{backgroundColor: 'transparent', borderWidth: 0}}
          title={<Text>I accept the terms of use & privacy policy</Text>}
          checked={accept}
          onPress={() => setAccept(!accept)}
        />
        <CButton title="Sign Up" onPress={signUpHandler} style={{margin: 20}} />
      </Card.Content>
    </Card>
  );
};

export {SignUp};
