import {ImageSourcePropType} from 'react-native';

export type ButtonType = undefined | 'secondary' | 'light';

export interface HomeScreenData {
  content: string[];
  heading: string;
  imageSource: ImageSourcePropType;
}
